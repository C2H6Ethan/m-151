﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m_151.Models
{
    public class Product
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public string ProductImage { get; set; }
        public virtual Cart Chart { get; set; }
    }
}
