﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m_151.Models
{
    public class Cart
    {
        public int Id { get; set; }
        public List<User> Users { get; set; }
        public List<Product> Products { get; set; }
    }
}
