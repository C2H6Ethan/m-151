﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m_151.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public virtual Cart Chart { get; set; }
    }
}
