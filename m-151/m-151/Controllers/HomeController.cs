using m_151.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace m_151.Controllers
{
    public class HomeController : Controller
    {
        ProductDB db = new ProductDB();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            return View("Login");
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(User user)
        {
            if (db.Users.Any(u => u.Password == user.Password && u.Username == user.Username))
            {
                return View("Homepage");
            }
            return View("Login");
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(User user)
        {
            User newUser = new User();

            newUser.Username = user.Username;
            newUser.Password = user.Password;

            db.Users.Add(newUser);
            db.SaveChanges();
            
            return View("Homepage");
        }

        public IActionResult Homepage()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
